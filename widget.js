widgetNameIntr = function() {
    var widget = this;
    this.code = null;

    this.yourVar = {};
    this.yourFunc = function() {};

// вызывается один раз при инициализации виджета, в этой функции мы вешаем события на $(document)
    this.bind_actions = function(){
//пример $(document).on('click', 'selector', function(){});
    };

// вызывается каждый раз при переходе на страницу
    this.render = function() {
        // let twig = request('twigjs');
        if(AMOCRM.data.current_entity == 'leads'){
            let phone = $('div.js-linked-with-actions.js-linked-has-actions.js-linked-has-value[data-pei-code="phone"]');
            let email = $('div.js-linked-with-actions.js-linked-has-actions.js-linked-has-value[data-pei-code="email"]');

            for(let key = 0; key < phone.length; key++){
                let val = phone[key].children[0].children[0].value;
                let div = `<div class="tips-item js-tips-item js-cf-actions-item" data-type="search"><img src="https://img.icons8.com/doodle/18/000000/google-logo.png" style="margin-right: 5px;"> <a href="https://yandex.ru/search/?text=${val}">Нагуглить</a> </div>`
                phone[key].children[1].children[0].innerHTML += div
            }

            for(let key = 0; key < email.length; key++){
                let val = email[key].children[0].children[1].value;
                let div = `<div class="tips-item js-tips-item js-cf-actions-item" data-type="search"><img src="https://img.icons8.com/doodle/18/000000/google-logo.png" style="margin-right: 5px;"> <a href="https://yandex.ru/search/?text=${val}">Нагуглить</a> </div>`
                email[key].children[1].children[0].innerHTML += div
            }
        }
    };

// вызывается один раз при инициализации виджета, в этой функции мы загружаем нужные данные, стили и.т.п
    this.init = function(){

    };

// метод загрузчик, не изменяется
    this.bootstrap = function(code) {
        widget.code = code;
// если frontend_status не задан, то считаем что виджет выключен
// var status = yadroFunctions.getSettings(code).frontend_status;
        var status = 1;

        if (status) {
            widget.init();
            widget.render();
            widget.bind_actions();
            $(document).on('widgets:load', function () {
                widget.render();
            });
        }
    }
};
// создание экземпляра виджета и регистрация в системных переменных Yadra
// widget-name - ИД и widgetNameIntr - уникальные названия виджета
yadroWidget.widgets['linkUsers'] = new widgetNameIntr();
yadroWidget.widgets['linkUsers'].bootstrap('linkUsers');